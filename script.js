//Créez une fonction qui prend un tableau contenant uniquement des nombres et renvoie le premier élément.
//Exo 2
function getFirstValue(arr) {
	return arr[0];
}
var tab = [1,5,6,8,7,89,56];

//Il existe un opérateur unique en JavaScript, capable de fournir le reste d’une opération de division. Deux nombres sont passés en tant que paramètres. Le premier paramètre divisé par le deuxième paramètre aura un reste, éventuellement zéro. Renvoyer cette valeur.
//Exo3
function remainder(x, y) {
	return x%y;
}

//Écrivez une fonction qui prend la base et la hauteur d’un triangle et sa surface.
//Exo4
function triArea(base, height) {
	return (base * height)/2;
}

//Créez une fonction qui prend et et trouve le périmètre d’un rectangle.
//Exo5
function findPerimeter(length, width) {
	return  2 * (length + width);
}

//Créez une fonction qui prend et retourne la puissance calculée.voltagecurrent
//Exo6
function circuitPower(voltage, current) {
	return voltage * current;
}

//Créez une fonction qui prend un nombre comme argument, incrémente le nombre de +1 et retourne le résultat.
//Exo7
function addition(num) {
	return num = num + 1;
}

//Créez une fonction qui recherche la plage maximale de la troisième arête d’un triangle, où les longueurs de côté sont toutes des entiers.
//Exo8
function nextEdge(side1, side2) {
	return (side1 + side2) - 1;
}

//Étant donné un polygone régulier à n côtés, on renvoie la somme totale des angles internes (en degrés).n
//Exo9
function sumPolygon(n) {
    if (n < 3) {
       return "Un polygone doit avoir au moins 3 côtés.";
   }
   const sommeAngles = (n - 2) * 180;
   return sommeAngles;
}

//Un étudiant apprenant JavaScript essayait de créer une fonction. Son code doit concaténer une chaîne passée avec string et la stocker dans une variable appelée . Il a besoin de votre aide pour corriger ce code.name"Edabit"result
//Exo10
function nameString(name){
	var b = "Edabit"
	var result = name + b
  	return result;
}

//Créez une fonction qui renvoie le nombre de valeurs contenues dans un tableau.true
//Exo11
function countTrue(arr) {
	 let compteur = 0;
  
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === true) {
      compteur++;
    }
  }
  
  return compteur;
}
var tab = [true, false, false, true, false];

//Écrivez une fonction qui renvoie la chaîne jointe par un espace et l’argument donné."something"" "a
//Exo12
function giveMeSomething(a) {
	return "something "+a;
}


//Créez une fonction qui prend l’âge en années et renvoie l’âge en jours.
//Exo13
function calcAge(age) {
	return age*365;
}

//Mubashir a créé une boucle infinie ! Aidez-le en corrigeant le code dans l’onglet code pour réussir ce défi. Regardez les exemples ci-dessous pour avoir une idée de ce que la fonction devrait faire.
//Exo14
function printArray(number) {
  var newArray = [];

  for(var i = 1; i <= number; i++) {
    newArray.push(i);
  }

  return newArray;
}

//Écrire une fonction qui convertit hours en secondes.
//Exo15
function howManySeconds(hours) {
	return hours*60*60;
}